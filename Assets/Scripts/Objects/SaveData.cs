using UnityEngine;

public static class SaveData 
{
    public static IFigureParametrs Parametrs;
    public static Color FigureColor;
    public static Figures CurrentFigure;
    public static bool IsUIWorking = false;    
}
