using UnityEngine;

public class FigureCreactor
{
    public static Mesh CreateParallelepiped(ParallelepipedParams parametrs)
    {
        MeshBuilder cubeBuilder = new MeshBuilder("Cube Mesh");

        Vector3 up = Vector3.up * parametrs.Height;
        Vector3 right = Vector3.right * parametrs.Width;
        Vector3 forward = Vector3.forward * parametrs.Length;

        Vector3 frontCorner = new Vector3(right.x * 0.5f, up.y * 0.5f, forward.z * 0.5f);
        Vector3 backCorner = -frontCorner;

        cubeBuilder.BuildQuad(backCorner, right, up, new Vector4(1f, 1f, 0f, -1f));       //back face
        cubeBuilder.BuildQuad(backCorner, up, forward, new Vector4(0f, 1f, 1f, -1f));     //left face
        cubeBuilder.BuildQuad(backCorner, forward, right, new Vector4(1f, 0f, 1f, -1f));  //down face

        cubeBuilder.BuildQuad(frontCorner, -up, -right, new Vector4(1f, 1f, 0f, 1f));     //front face
        cubeBuilder.BuildQuad(frontCorner, -forward, -up, new Vector4(0f, 1f, 1f, 1f));   //right face
        cubeBuilder.BuildQuad(frontCorner, -right, -forward, new Vector4(1f, 0f, 1f, 1f));//up face

        return cubeBuilder.Create();
    }

    public static Mesh CreatePrism(PrizmParams parametrs)
    {
        float angleInc = (Mathf.PI + Mathf.PI) / parametrs.CountEdge;
        Vector3[] downVertex = new Vector3[parametrs.CountEdge + 1];
        Vector3[] upVertex = new Vector3[parametrs.CountEdge + 1];

        for (int i = 0; i <= parametrs.CountEdge; i++)
        {
            var angle = angleInc * i;
            downVertex[i] = new Vector3(Mathf.Sin(angle), 0f, -Mathf.Cos(angle)) * parametrs.Radius;
            upVertex[i] = new Vector3(downVertex[i].x, parametrs.Height, downVertex[i].z);
        }

        MeshBuilder builder = new MeshBuilder("Prism");

        for (int i = 0; i < downVertex.Length - 1; i++)
        {
            Vector3 corner1 = downVertex[i];
            Vector3 corner2 = upVertex[i];
            Vector3 corner3 = downVertex[i + 1];

            builder.BuildQuad(corner1, corner3 - corner1, corner2 - corner1, new Vector4(1, 0, 0, 1));
        }

        builder.BuildCap(Vector3.up * parametrs.Height, parametrs.CountEdge, parametrs.Radius, true);
        builder.BuildCap(Vector3.zero, parametrs.CountEdge, parametrs.Radius, false);

        return builder.Create();
    }

    public static Mesh CreateSphere(SphereParams parametrs)
    {
        float heightSegmentCount = parametrs.SegmentCount * 0.5f;

        float angleInc = Mathf.PI / heightSegmentCount;

        MeshBuilder builder = new MeshBuilder("Sphere");

        for (int i = 0; i <= heightSegmentCount; i++)
        {
            float currentAngle = angleInc * i;

            Vector3 center = new Vector3(0, -Mathf.Cos(currentAngle) * parametrs.Radius, 0);

            var currentRadius = Mathf.Sin(currentAngle) * parametrs.Radius;

            float v = (float)i / heightSegmentCount;

            builder.BuildRing(parametrs.SegmentCount, center, currentRadius, v, i > 0);
        }

        return builder.Create();
    }

    public static Mesh CreateCapsule(CapsuleParams parametrs)
    {
        MeshBuilder builder = new MeshBuilder("Capsule");

        float diameter = parametrs.Radius + parametrs.Radius;

        if (parametrs.Height < diameter) parametrs.Height = diameter;

        int heightSegmentCount = 3;

        float halfHeightCylinderBody = (parametrs.Height - diameter) * 0.5f;
        Vector3 downCornerCylinder = new Vector3(0, -halfHeightCylinderBody, 0);

        CreateDownHemisphere(builder, Vector3.down * halfHeightCylinderBody, parametrs.SegmentCount, parametrs.Radius);
        CreateCylinderBody(builder, downCornerCylinder, halfHeightCylinderBody, parametrs.Radius, heightSegmentCount, parametrs.SegmentCount);
        CreateUpHemisphere(builder, Vector3.up * halfHeightCylinderBody, parametrs.SegmentCount, parametrs.Radius);

        return builder.Create();
    }

    private static void CreateDownHemisphere(MeshBuilder builder, Vector3 center, int radialSegmentCount, float radius)
    {
        int heightSegmentCount = (int)(radialSegmentCount * 0.5);

        if (heightSegmentCount % 2 != 0) heightSegmentCount--;

        int halfHeightSegmentCount = (int)(heightSegmentCount * 0.5f);

        float angleInc = Mathf.PI / heightSegmentCount;

        for (int i = 0; i < halfHeightSegmentCount; i++)
        {
            float currentAngl = angleInc * i;
            Vector3 currentCenter = center;
            currentCenter.y    -= Mathf.Cos(currentAngl) * radius;
            float currentRadius = Mathf.Sin(currentAngl) * radius;

            float v = (float)i / heightSegmentCount;

            builder.BuildRing(radialSegmentCount, currentCenter, currentRadius, v, i > 0);
        }
    }

    private static void CreateUpHemisphere(MeshBuilder builder, Vector3 center, int radialSegmentCount, float radius)
    {
        int heightSegmentCount = (int)(radialSegmentCount * 0.5);

        if (heightSegmentCount % 2 != 0) heightSegmentCount--;

        int halfHeightSegmentCount = (int)(heightSegmentCount * 0.5f);

        float angleInc = Mathf.PI / heightSegmentCount;

        for (int i = halfHeightSegmentCount + 1; i <= heightSegmentCount; i++)
        {
            float currentAngl = angleInc * i;
            Vector3 currentCenter = center;
            currentCenter.y    -= Mathf.Cos(currentAngl) * radius;
            float currentRadius = Mathf.Sin(currentAngl) * radius;

            float v = (float)i / heightSegmentCount;

            builder.BuildRing(radialSegmentCount, currentCenter, currentRadius, v, true);
        }
    }

    private static void CreateCylinderBody(MeshBuilder builder, Vector3 downCorner, float height, float radius, int heightSegmentCount, int radialSegmentCount)
    {
        float heightInc = height / heightSegmentCount;

        for (int i = 0; i <= heightSegmentCount; i++)
        {
            Vector3 center = downCorner + Vector3.up * heightInc * i;
            float v = (float)i / heightSegmentCount;
            builder.BuildRing(radialSegmentCount, center, radius, v, true);
        }
    }
}