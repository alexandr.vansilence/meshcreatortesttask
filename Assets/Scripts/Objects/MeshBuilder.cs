using System.Collections.Generic;
using UnityEngine;

public class MeshBuilder 
{
    private const float DoublePi = Mathf.PI + Mathf.PI;

    private List<Vector4> _tangents;
    private List<Vector3> _vertices;
    private List<Vector3> _normals;
    private List<Vector2> _uv;
    private List<int> _indexes;

    string name;

    public MeshBuilder(string name)
    {
        _tangents = new List<Vector4>();
        _vertices = new List<Vector3>();
        _normals = new List<Vector3>();
        _uv = new List<Vector2>();
        _indexes = new List<int>();

        this.name = name;
    }

    public Mesh Create()
    {
        Mesh mesh = new Mesh()
        {
            vertices = _vertices.ToArray(),
            triangles = _indexes.ToArray(),
            normals = _normals.ToArray(),
            tangents = _tangents.ToArray(),
            uv = _uv.ToArray(),
            name = this.name,
        };

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        return mesh;
    }

    private void AddVertices(Vector3 vertex, Vector3 normal, Vector2 UV, Vector4 tangent)
    {
        _vertices.Add(vertex);
        _normals.Add(normal);
        _uv.Add(UV);
        _tangents.Add(tangent);
    }

    private void AddTriangle(int index0, int index1, int index2)
    {
        _indexes.Add(index0);
        _indexes.Add(index1);
        _indexes.Add(index2);
    }

    public void BuildQuad(Vector3 corner, Vector3 widthDir, Vector3 lengthDir, Vector4 tangent)
    {
        Vector3 normal = Vector3.Cross(lengthDir, widthDir).normalized;

        AddVertices(corner, normal, Vector2.zero, tangent);
        AddVertices(corner + lengthDir, normal, Vector2.up, tangent);
        AddVertices(corner + lengthDir + widthDir, normal, Vector2.one, tangent);
        AddVertices(corner + widthDir, normal, Vector2.right, tangent);

        int baseIndex = _vertices.Count - 4;

        RenderQuad(baseIndex, baseIndex + 1, baseIndex + 2, baseIndex + 3);
    }

    private void RenderQuad(int index0, int index1, int index2, int index3)
    {
        AddTriangle(index0, index1, index2);
        AddTriangle(index0, index2, index3);
    }

    public void BuildRing(int segmentCount, Vector3 center, float radius, float v, bool buildTriangles)
    {
        float angleInc = DoublePi / segmentCount;

        for (int i = 0; i <= segmentCount; i++)
        {
            float angle = angleInc * i;

            Vector3 unitPosition = GetUnitPosition(angle);

            Vector3 vertexPosition = center + unitPosition * radius;
            Vector2 UV = new Vector2((float)i / segmentCount, v);
            Vector4 tangent = new Vector4(-unitPosition.z, 0, unitPosition.x, -1);

            AddVertices(vertexPosition, unitPosition.normalized, UV, tangent);

            if (i > 0 && buildTriangles)
            {
                int baseIndex = _vertices.Count - 1;
                int vertsPerRow = segmentCount + 1;

                int index0 = baseIndex;
                int index1 = baseIndex - 1;
                int index2 = baseIndex - vertsPerRow;
                int index3 = index1    - vertsPerRow;

                RenderQuad(index1, index0, index2, index3);
            }
        }
    }  

    public void BuildCap(Vector3 center, int segmentCount, float radius, bool isDirectionUp = true)
    {
        Vector3 normal = isDirectionUp ? Vector3.up : Vector3.down;

        AddVertices(center, normal, new Vector2(0.5f, 0.5f), new Vector4(1, 0, 0, 1));

        int centerVertexIndex = _vertices.Count - 1;
        float angleInc = DoublePi / segmentCount;

        for (int i = 0; i <= segmentCount; i++)
        {
            float angle = angleInc * i;

            Vector3 unitPosition = GetUnitPosition(angle);

            Vector3 vertexPosition = center + unitPosition * radius;
            Vector2 UV = new Vector2(unitPosition.x + 1.0f, unitPosition.z + 1.0f) * 0.5f;
            Vector4 tangent = new Vector4(1,0,0,1);

            AddVertices(vertexPosition, normal, UV, tangent);

            if (i > 0)
            {
                int baseIndex = _vertices.Count - 1;

                if (isDirectionUp)
                    AddTriangle(centerVertexIndex, baseIndex, baseIndex - 1);
                else
                    AddTriangle(centerVertexIndex, baseIndex - 1, baseIndex);
            }
        }
    }

    private Vector3 GetUnitPosition(float angle)
    {
        return new Vector3(Mathf.Sin(angle), 0, -Mathf.Cos(angle));
    }
}