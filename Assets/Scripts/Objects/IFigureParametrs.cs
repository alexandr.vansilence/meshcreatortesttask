public interface IFigureParametrs { }

public class ParallelepipedParams : IFigureParametrs
{
    public float Length;
    public float Width;
    public float Height;
}

public class SphereParams : IFigureParametrs
{
    public float Radius;
    public int SegmentCount;
}

public class PrizmParams : IFigureParametrs
{
    public float Height;
    public float Radius;
    public int CountEdge;
}

public class CapsuleParams : IFigureParametrs
{
    public float Radius;
    public float Height;
    public int SegmentCount;
}
