using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public class SimpleProceduralMesh : MonoBehaviour
{
    private MeshFilter _meshFilter;
    private MeshCollider _collider;

    public IFigureParametrs ParametrsFigure { get; private set; }
    public Figures TypeFigure { get; private set; } = Figures.None;

    public void BuildMesh(IFigureParametrs parametrs, Figures type)
    {
        TypeFigure = type;
        ParametrsFigure = parametrs;
        Initialize();
        StartRenderingFigure();
        _collider.sharedMesh = _meshFilter.sharedMesh;
        SaveData.IsUIWorking = false;
    }

    private void StartRenderingFigure()
    {
        switch (TypeFigure)
        {
            case Figures.Parallelepiped: _meshFilter.mesh = FigureCreactor.CreateParallelepiped(ParametrsFigure as ParallelepipedParams); break;
            case Figures.Prizm: _meshFilter.mesh = FigureCreactor.CreatePrism(ParametrsFigure as PrizmParams); break;
            case Figures.Capsule: _meshFilter.mesh = FigureCreactor.CreateCapsule(ParametrsFigure as CapsuleParams); break;
            case Figures.Sphere: _meshFilter.mesh = FigureCreactor.CreateSphere(ParametrsFigure as SphereParams); break;
        }
    }

    private void Initialize()
    {
        if(_meshFilter == null)
        {
            _meshFilter = GetComponent<MeshFilter>();
        }

        if(_collider == null)
        {
            _collider = GetComponent<MeshCollider>();
        }
    }
}
