using UnityEngine;
using UnityEngine.UI;

public class ChoiceFigurePanel : MonoBehaviour
{

    [SerializeField] private GameObject panelChoiceFigure;
    [SerializeField] private GameObject buttonFigureChoice;

    public void OpenPanelFigure()
    {
        SaveData.IsUIWorking = true;
        buttonFigureChoice.SetActive(false);
        panelChoiceFigure.SetActive(true);
    }

    public void ClosePanelFigure()
    {
        buttonFigureChoice.SetActive(true);
        CloseOnlyPanel();
    }

    public void CloseOnlyPanel()
    {
        panelChoiceFigure.SetActive(false);
    }
}
