using UnityEngine;
using UnityEngine.UI;

public enum Figures { None, Parallelepiped, Prizm, Capsule, Sphere }

public class ParametrsFigure : MonoBehaviour
{
    private enum Settings { Create, Modify }

    private Settings _typeParametr = Settings.Create;
    private GameObject _paralSettings;
    private GameObject _capsuleSettings;
    private GameObject _prizmSettings;
    private GameObject _sphereSettings;
    private InputField[] _inputText;
    private SimpleProceduralMesh _figureModify;

    [SerializeField] private GameObject panelSettings;
    [SerializeField] private GameObject panelColor;

    private void Start()
    {
        _paralSettings = panelSettings.GetComponentInChildren<ParalSet>(true).gameObject;
        _capsuleSettings = panelSettings.GetComponentInChildren<CapsuleSet>(true).gameObject;
        _prizmSettings = panelSettings.GetComponentInChildren<PrizmSet>(true).gameObject;
        _sphereSettings = panelSettings.GetComponentInChildren<SphereSet>(true).gameObject;
    }

    public void OpenParalSettings()
    {
        OpenSettings(_paralSettings, Figures.Parallelepiped, 3);
    }

    public void OpenSphereSettings()
    {
        OpenSettings(_sphereSettings, Figures.Sphere, 2);
    }

    public void OpenCapsuleSettings()
    {
        OpenSettings(_capsuleSettings, Figures.Capsule, 3);
    }

    public void OpenPrizmSettings()
    {
        OpenSettings(_prizmSettings, Figures.Prizm, 3);
    }

    public void OpenSettings(GameObject settingsPanel, Figures type, int countParametrs)
    {
        panelSettings.SetActive(true);
        settingsPanel.SetActive(true);
        _inputText = settingsPanel.GetComponentsInChildren<InputField>(true);
        SaveData.CurrentFigure = type;
        _typeParametr = Settings.Create;

        for (int i = 0; i < countParametrs; i++)
        {
            SetTextInInputText(i, "");
        }
    }

    public void ModifyFigure(SimpleProceduralMesh figure)
    {
        SaveData.IsUIWorking = true;
        _typeParametr = Settings.Modify;
        SaveData.CurrentFigure = figure.TypeFigure;
        panelSettings.SetActive(true);

        switch (SaveData.CurrentFigure)
        {
            case Figures.Parallelepiped:
                _paralSettings.SetActive(true);
                _inputText = _paralSettings.GetComponentsInChildren<InputField>(true);
                ParallelepipedParams paralParams = figure.ParametrsFigure as ParallelepipedParams;
                SetTextInInputText(0, paralParams.Length.ToString());
                SetTextInInputText(1, paralParams.Width.ToString());
                SetTextInInputText(2, paralParams.Height.ToString());
                break;

            case Figures.Sphere:
                _sphereSettings.SetActive(true);
                _inputText = _sphereSettings.GetComponentsInChildren<InputField>(true);
                SphereParams sphereParams = figure.ParametrsFigure as SphereParams;
                SetTextInInputText(0, sphereParams.Radius.ToString());
                SetTextInInputText(1, sphereParams.SegmentCount.ToString());
                break;

            case Figures.Capsule:
                _capsuleSettings.SetActive(true);
                _inputText = _capsuleSettings.GetComponentsInChildren<InputField>(true);
                CapsuleParams capsuleParams = figure.ParametrsFigure as CapsuleParams;
                SetTextInInputText(0, capsuleParams.Radius.ToString());
                SetTextInInputText(1, capsuleParams.Height.ToString());
                SetTextInInputText(2, capsuleParams.SegmentCount.ToString());
                break;

            case Figures.Prizm:
                _prizmSettings.SetActive(true);
                _inputText = _prizmSettings.GetComponentsInChildren<InputField>(true);
                PrizmParams prizmParams = figure.ParametrsFigure as PrizmParams;
                SetTextInInputText(0, prizmParams.Radius.ToString());
                SetTextInInputText(1, prizmParams.Height.ToString());
                SetTextInInputText(2, prizmParams.CountEdge.ToString());
                break;
        }

        _figureModify = figure;
    }

    public void AcceptButton()
    {
        switch (SaveData.CurrentFigure)
        {
            case Figures.Parallelepiped:
                ParallelepipedParams parametrsParal = new ParallelepipedParams();
                parametrsParal.Length = float.Parse(GetTextInInputText(0));
                parametrsParal.Width = float.Parse(GetTextInInputText(1));
                parametrsParal.Height = float.Parse(GetTextInInputText(2));
                SaveData.Parametrs = parametrsParal;
                _paralSettings.SetActive(false);
                break;

            case Figures.Prizm:
                PrizmParams parametrsPrizm = new PrizmParams();
                parametrsPrizm.Radius = float.Parse(GetTextInInputText(0));
                parametrsPrizm.Height = float.Parse(GetTextInInputText(1));
                parametrsPrizm.CountEdge = int.Parse(GetTextInInputText(2));
                SaveData.Parametrs = parametrsPrizm;
                _prizmSettings.SetActive(false);
                break;

            case Figures.Capsule:
                CapsuleParams parametrsCapsule = new CapsuleParams();
                parametrsCapsule.Radius = float.Parse(GetTextInInputText(0));
                parametrsCapsule.Height = float.Parse(GetTextInInputText(1));
                parametrsCapsule.SegmentCount = int.Parse(GetTextInInputText(2));
                SaveData.Parametrs = parametrsCapsule;
                _capsuleSettings.SetActive(false);
                break;

            case Figures.Sphere:
                SphereParams parametrsSphere = new SphereParams();
                parametrsSphere.Radius = float.Parse(GetTextInInputText(0));
                parametrsSphere.SegmentCount = int.Parse(GetTextInInputText(1));
                SaveData.Parametrs = parametrsSphere;
                _sphereSettings.SetActive(false);
                break;
        }

        if (_typeParametr == Settings.Create)
        {
            panelColor.SetActive(true);
        }
        else if (_typeParametr == Settings.Modify)
        {
            _figureModify.BuildMesh(SaveData.Parametrs, SaveData.CurrentFigure);
        }

        panelSettings.SetActive(false);
    }

    private void GetText<T>(int num)
    {

    }

    private string GetTextInInputText(int index)
    {
        return _inputText[index].text;
    }

    private void SetTextInInputText(int index, string value)
    {
        _inputText[index].text = value;
    }
}