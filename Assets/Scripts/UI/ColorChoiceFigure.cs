using UnityEngine;

public class ColorChoiceFigure : MonoBehaviour
{
    private Camera _camera;

    [SerializeField] private GameObject prefab;
    [SerializeField] private GameObject buttonFigure;
    [SerializeField] private GameObject colorPanel;

    private void Awake()
    {
        _camera = Camera.main;
    }

    public void ChoiceRed()
    {
        SaveData.FigureColor = Color.red;
    }

    public void ChoiceYellow()
    {
        SaveData.FigureColor = Color.yellow;
    }

    public void ChoiceGreen()
    {
        SaveData.FigureColor = Color.green;
    }

    public void ChoiceBlue()
    {
        SaveData.FigureColor = Color.cyan;
    }

    public void ChoiceDarkBlue()
    {
        SaveData.FigureColor = Color.blue;
    }

    public void ChoiceMagenta()
    {
        SaveData.FigureColor = Color.magenta;
    }

    public void CreateFigure()
    {
        GameObject figure = Instantiate(prefab, _camera.transform.position + (Vector3.forward * 10), Quaternion.identity);
        figure.GetComponent<SimpleProceduralMesh>().BuildMesh(SaveData.Parametrs, SaveData.CurrentFigure);
        figure.GetComponent<Renderer>().material = new Material(Shader.Find("Diffuse"));
        figure.GetComponent<Renderer>().material.SetColor("_Color", SaveData.FigureColor);
        buttonFigure.SetActive(true);
        colorPanel.SetActive(false);
    }
}
