using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1000f;
    [SerializeField] private float rotationSpeed = 300f;
    [SerializeField] private ParametrsFigure parametrsFigure;

    private Vector2 _rotation = Vector2.zero;
    private Vector3 _moveVector = Vector3.zero;

    private Rigidbody _rigidbody;
    private Camera _camera;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.useGravity = false;
        _camera = Camera.main;
        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            CamerControll();

            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            if (SaveData.IsUIWorking == false)
            {
                FigureChange();
            }

            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void CamerControll()
    {
        _rotation += new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X")) * Time.deltaTime;

        transform.eulerAngles = _rotation * rotationSpeed;

        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");

        _moveVector = new Vector3(x, 0, z) * moveSpeed * Time.deltaTime;
    }

    private void FigureChange()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                GameObject objecthit = hit.collider.gameObject;

                if (objecthit.GetComponent<SimpleProceduralMesh>())
                {                   
                    parametrsFigure.ModifyFigure(objecthit.GetComponent<SimpleProceduralMesh>());                    
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(1))
        {
            _rigidbody.velocity = transform.TransformDirection(_moveVector);
        }
        else
        {
            _rigidbody.velocity = Vector3.zero;
        }
    }
}